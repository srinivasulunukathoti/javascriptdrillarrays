// Do NOT use .map, to complete this function.
    // How map works: Map calls a provided callback function once for each element in an array, in order, and functionructs a new array from the res .
    // Produces a new array of values by mapping each value in list through a transformation function (iteratee).
    // Return the new array.
function map(element , callBack) {
    //check array or not
    if (Array.isArray(element)) {
        //creating empty array to store
        let mapArray = [];
        for (let index = 0; index < element.length; index++) {
            if(element[index] !== undefined){
                //Here calling callback function and pushing the data.
               mapArray.push(callBack(element[index],index,element));
            }
        }
        return mapArray;
    } else {
        return "Not a array";
    }
};
//exporting the code
module.exports = map;