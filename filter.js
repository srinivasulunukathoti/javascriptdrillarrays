 // Do NOT use .filter, to complete this function.
    // Similar to `find` but you will return an array of all elements that passed the truth test
    // Return an empty array if no elements pass the truth test

function filter(element , callBack) {
    //check array or not
    if (Array.isArray(element)) {
        //creating empty array to store
        let filterData = [];
        for (let index = 0; index < element.length; index++) {
            //check the condition true or false
            if (callBack(element[index]) !== undefined) {
                //Pushing the data into array.
                filterData.push(callBack(element[index]));
            }
        }
        return filterData;
    } else {
        return "Not a array"
    }
};
//exporting the code
module.exports = filter;