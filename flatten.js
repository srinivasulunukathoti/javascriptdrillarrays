 // Flattens a nested array (the nesting can be to any depth).
    // Hint: You can solve this using recursion.
    // Example: flatten([1, [2], [3, [[4]]]]); => [1, 2, 3, 4];

function flatten(element) {
    //To store data
     let resultArray = [];
     //converting  to nested array as single array.
      element.forEach(element => {
        if(Array.isArray(element)){
            resultArray.push(...flatten(element));
        }else{
            resultArray.push(element);
        }
      });
      return resultArray;
};
//exporting the code
module.exports = flatten;