 // Do NOT use forEach to complete this function.
    // Iterates over a list of elements, yielding each in turn to the `cb` function.
    // This only needs to work with arrays.
   
function each(element , callBack) {
    //check array or not
    if (Array.isArray(element)) {
        for (let index = 0; index < element.length; index++) {
            // calling callbcak function
            callBack(element);
        }
        
    } else {
        return "Not a array";
    }
};
//exporting the code
module.exports  = each;
    
