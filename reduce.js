// Do NOT use .reduce to complete this function.
    // How reduce works: A reduce function combines all elements into a single value going from left to right.
    // Elements will be passed one by one into `cb` along with the `startingValue`.
    // `startingValue` should be the first argument passed to `cb` and the array element should be the second argument.
    // `startingValue` is the starting value.  If `startingValue` is undefined then make `elements[0]` the initial value.

function reduce(element ,callBack,startingValue) {
    //check array or not
    if (Array.isArray(element)) {
        let accumulator ;
        let indexValue ;
        if(startingValue!== undefined)
        {
            // consider as starting value value is accumulator value.
         accumulator = startingValue;
         indexValue = 0;
        }else{
            // consider as array initial value is accumulator value.
         accumulator = element[0];
         indexValue = 1;
        }
        for (let index = indexValue; index < element.length; index++) {
            //calling callback function with arguments
            accumulator =callBack(accumulator , element[index], element);
        }
        return  sum = "sum : "+[accumulator];
    } else {
        return "Not a array";
    }
}
//exporting the code
module.exports = reduce;