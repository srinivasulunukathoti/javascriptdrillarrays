const find = require('../find');
const element = require('../data');

function callBack(element ,index ,array) {
    for (let index = 0; index < array.length; index++) {
        const data = array[index];
        if (element === data) {
            return data;
        }
    }
}

try {
    const result = find(element ,callBack);
    console.log(result);
} catch (error) {
    console.error(error);
}