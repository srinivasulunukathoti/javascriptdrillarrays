const flatten = require('../flatten');
const element =  [1, [2], [[3]], [[[4]]]];

try {
    const result =flatten(element);
    console.log(result);
} catch (error) {
    console.error(error);
}