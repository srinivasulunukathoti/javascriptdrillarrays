const each = require('../each');
const element = require('../data');

function callBack(element , index , array) {
    console.log(element);
};

try {
   each(element , callBack);
   
} catch (error) {
    console.error(error);
}