// Do NOT use .includes, to complete this function.
    // Look through each value in `elements` and pass each element to `cb`.
    // If `cb` returns `true` then return that element.
    // Return `undefined` if no elements pass the truth test.

function find(element , callBack) {
    //check array or not
    if (Array.isArray(element)) {
        let find ;
        for (let index = 0; index < element.length; index++) {
            //calling the callBack funtion by passing parameters.
            find = callBack(element[index],index, element);
        }
        return find;
    } else {
        return "Not a array";
    }
};
//exporting the code
module.exports = find;